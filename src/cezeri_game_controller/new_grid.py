#!/usr/bin/env python
zone_size = 5
grid_legth = 100
grid_width = 100

helipads = [
    (-177.5, 172.5, 5.96129),
    (102.5, 167.5, 0.130374),
    (52.5, 157.5, 0.130374),
    (137.5, 152.5, 0.130374),
    (-117.5, 102.5, 6.91252),
    (72.5, 112.5, 0.130374),
    (52.5, 57.5, 8.33016),
    (92.5, 47.5, 8.94583),
    (-122.5, 27.5, 8.0838),
    (127.5, 12.5, 8.32921),
    (-192.5, 2.5, 8.63281),
    (57.5, -22.5, 8.08298),
    (-92.5, -27.5, 8.09477),
    (-187.5, -32.5, 23.7387),
    (-77.5, -47.5, 11.9177),
    (-172.5, -67.5, 8.37982),
    (57.5, -77.5, 8.63132),
    (92.5, -77.5, 8.08445),
    (7.5, -102.5, 11.5849),
]

class Zone:
    def __init__(self, id):
        self.id = id
        self.helipad = False
        self.non_fly_zone = False
        self.rain = False
        self.wind = False
        self.fog = False
        self.landable = False
        self.elevation = 0.0

    

    def __str__(self):
        message = {
            "helipad": self.helipad,
            "non_fly_zone": self.non_fly_zone,
            "rain": self.rain,
            "wind": self.wind,
            "fog": self.fog,
            "landable": self.landable,
            "elevation": self.elevation,
        }
        return str(message)

class Grid:
    def __init__(self, zone_size, grid_length, grid_width):
        self.zone_size = zone_size
        self.grid_legth = grid_length
        self.grid_width = grid_width
        self.grid = [Zone(i) for i in range(self.grid_legth*self.grid_width)]

        self.init_helipads(helipads)

    def get_zone(self, col, row):
        if row < 0:
            r = int(self.grid_legth/2) - row - 1
        else: 
            r = int(self.grid_legth/2) - row
        if col < 0:
            c = int(self.grid_legth/2) + col
        else: 
            c = int(self.grid_legth/2) + col - 1
        return self.grid[(self.grid_width * r) + c]

    def get_zone_by_id(self, id):
        return self.grid[id]

    def get_zone_center_point(self, col, row):
        pos_x = (col/abs(col))*((self.zone_size * abs(col)) - (float(self.zone_size)/2))
        pos_y = (row/abs(row))*((self.zone_size * abs(row)) - (float(self.zone_size)/2))

        return pos_x, pos_y

    def get_zone_from_pos(self, pos_x, pos_y):
        col = (pos_x/abs(pos_x))*int((abs(pos_x) + float(self.zone_size))/float(self.zone_size)) 
        row = (pos_y/abs(pos_y))*int((abs(pos_y) + float(self.zone_size))/float(self.zone_size))

        return col, row

    def init_helipads(self, helipads):
        self.helipad_grid = []
        for helipad_pos in helipads:
            zone = self.get_zone_from_pos(helipad_pos[0], helipad_pos[1])
            zone.helipad = True

            self.helipad_grid.append(zonez)
        