#!/usr/bin/env python

import rospy
from cezeri_game_controller.grid_map import Grid, Zone, zone_size, grid_legth, grid_width
from cezeri_game_controller.setup import RosSetup, RidersSetup
from cezeri_game_controller import scenarios
from quadrotor_description.srv import Scenario
from cezeri_game_controller.srv import InitRobot
from cezeri_game_controller.msg import UpdateRobot
from std_msgs.msg import String


class GameController:
    def __init__(self, robot_name):
        self.robot_name = robot_name
        self.robot_status = "Paused"

        self.time_passed = 0.0
        self.start_time = 0.0

        self.map = Grid(zone_size, grid_legth, grid_width)
        self.ros_setup = RosSetup('game_controller', 10)
        self.riders_setup = RidersSetup()

        rospy.Service('/robot_init', InitRobot, self.init_robot)

        self.metrics = {
            'Status': self.robot_status,
            'Time': self.time_passed,
            'Score': 'N/A',
        }

        self.scenario = scenarios.ScenarioParent(self.robot_name, self.ros_setup, self.map)
        self.contact_sub = rospy.Subscriber('/{}/contact'.format(self.robot_name), String, self.scenario.on_contact)


        while ~rospy.is_shutdown():
            if self.robot_status == 'Running':
                self.time_passed = rospy.get_time() - self.start_time

            self.publish_metrics()
            self.ros_setup.rate.sleep()
            self.update_robot()

            if self.robot_status == "Stop":
                break

    def publish_metrics(self):
        self.metrics['Status'] = self.robot_status
        self.metrics['Time'] = self.time_passed
        self.ros_setup.publish_metrics(self.metrics)


    def init_scenario(self, scenario):
        if scenario != self.scenario.num:
            self.contact_sub.unregister()

            if scenario == 1:
                self.scenario = scenarios.Scenario1(self.robot_name, self.ros_setup, self.map)
            elif scenario == 2:
                self.scenario = scenarios.Scenario2(self.robot_name, self.ros_setup, self.map)
            elif scenario == 3:
                self.scenario = scenarios.Scenario3(self.robot_name, self.ros_setup, self.map)
            elif scenario == 5:
                self.scenario = scenarios.Scenario5(self.robot_name, self.ros_setup, self.map)

            self.contact_sub = rospy.Subscriber('/{}/contact'.format(self.robot_name), String, self.scenario.on_contact)


    def init_robot(self, req):
        self.init_scenario(req.scenario)
        response = self.scenario.init_robot(req)
        self.robot_status = response.message

        return response

    def update_robot(self):
        msg = self.scenario.update_robot()
        self.ros_setup.robot_update_pub.publish(msg)