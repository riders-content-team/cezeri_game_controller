#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
import os
import requests
import subprocess
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.msg import ModelState
from cezeri_game_controller.msg import UpdateRobot


class RosSetup:
    def __init__(self, node_name, update_rate):
        rospy.init_node(node_name)
        self.rate = rospy.Rate(update_rate)
        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size = 10)
        self.robot_update_pub = rospy.Publisher("/robot_update", UpdateRobot, queue_size=10)

        self.init_services()

    def publish_metrics(self, metrics):
        self.metric_pub.publish(json.dumps(metrics, sort_keys = True))

    def init_services(self):
        try:
            rospy.wait_for_service("/gazebo/get_model_state", 3)
            self.get_model_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            self.set_model_state = rospy.Publisher("/gazebo/set_model_state", ModelState, queue_size = 10)

        except(rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))


    def set_model_pose(self, model_name, x, y, z):
        state_msg = ModelState()

        state_msg.model_name = model_name
        state_msg.pose.position.x = x
        state_msg.pose.position.y = y
        state_msg.pose.position.z = z

        self.set_model_state.publish(state_msg)

class RidersSetup:
    def __init__(self):
        self.git_dir = '/workspace/src/.git'
        commit_id = ''
        riders_project_id = os.environ.get('RIDERS_PROJECT_ID', '')

        # RIDERS_COMMIT_ID
        git_dir_exists = os.path.isdir(self.git_dir)
        if git_dir_exists:

            popen = subprocess.Popen(['git', 'remote', 'get-url', 'origin'], cwd=self.git_dir, stdout=subprocess.PIPE)
            remote_url, error_remote_url = popen.communicate()

            popen = subprocess.Popen(['git', 'name-rev', '--name-only', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            branch_name, error_branch_name = popen.communicate()

            popen = subprocess.Popen(['git', 'rev-parse', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            commit_id, error_commit_id = popen.communicate()

            if error_remote_url is None and remote_url is not None:
                remote_url = remote_url.rstrip('\n').rsplit('@')
                user_name = remote_url[0].rsplit(':')[1][2:]
                remote_name = remote_url[1]

        self.result_metrics = {
            'Score' : 'N/A',
            'commit_id': commit_id.rstrip('\n'),
        }


    def send_to_api(self, score=0.0, disqualified=False,  **kwargs):
        simulation_id = os.environ.get('RIDERS_SIMULATION_ID', None)
        args = score, disqualified
        try:
            if simulation_id:
                self.send_simulation_results(simulation_id, *args, **kwargs)
            else:
                self.send_branch_results(*args, **kwargs)
        except Exception as e:
            rospy.loginfo('Exception occurred while sending metric: %s', e)
        

    def send_simulation_results(self, simulation_id, score, disqualified, **kwargs):
        host = os.environ.get('RIDERS_HOST', None)
        token = os.environ.get('RIDERS_SIMULATION_AUTH_TOKEN', None)
        create_round_url = '%s/api/v1/simulation/%s/rounds/' % (host, simulation_id)
        kwargs['score'] = kwargs.get('score', score)
        kwargs['disqualified'] = kwargs.get('disqualified', disqualified)
        data = {
            'metrics': json.dumps(kwargs)
        }
        # send actual data
        requests.post(create_round_url, {}, data, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


    def send_branch_results(self, score, disqualified, **kwargs):
        rsl_host = 'http://localhost:8059'
        # metrics endpoint stores data differently compared to simulation endpoint,
        # hence we change how we send it
        info = {
            'score': score,
            'disqualified': disqualified,
        }
        info.update(kwargs)
        url = '%s/events' % rsl_host
        data = {
            'event': 'metrics',
            'info': json.dumps(info)
        }

        requests.post(url, {}, data, headers={
            'Content-Type': 'application/json',
        })