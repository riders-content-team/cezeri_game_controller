#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from quadrotor_description.srv import DroneSensorSetup, DroneSensorSetupRequest, Scenario, ScenarioResponse
from cezeri_game_controller.srv import InitRobotResponse
from cezeri_game_controller.msg import UpdateRobot
from cezeri_game_controller.grid_map import helipads
import random

class ScenarioParent(object):
    def __init__(self, robot_name, ros_setup, map):
        self.num = 0
        self.robot_name = robot_name
        self.robot_status = 'Paused'
        self.map = map
        self.ros_setup = ros_setup

        self.sensor_setup = DroneSensorSetupRequest()
        self.sensor_setup.gnss_status = False
        self.sensor_setup.gnss_mix_status = False
        self.sensor_setup.barometer_status = False
        self.sensor_setup.radar_status = False
        self.sensor_setup.imu_status = False
        self.sensor_setup.imu_noise = False
        self.sensor_setup.lidar_status = False
        self.sensor_setup.magnetometer_status = False
        self.sensor_setup.magnetometer_noise = False
        self.sensor_setup.battery_status = False
        self.sensor_setup.battery_heat = False
        self.sensor_setup.motor_status = False
        self.sensor_setup.motor_heat = False

        self.destination = [0, 0]

    def init_sensors(self, robot_name):
        try:
            rospy.wait_for_service("/{}/{}/setup_sensor_data".format(robot_name, robot_name), 5)
            self.setup_drone_sensors = rospy.ServiceProxy("/{}/{}/setup_sensor_data".format(robot_name, robot_name), DroneSensorSetup)

        except(rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("service call failed: %s" % (e,))

    def on_contact(self, data):
        if "helipad" in data.data:
            robot_state = self.ros_setup.get_model_state("quadrotor", "")
            robot_col, robot_row = self.map.get_grid_position(
                robot_state.pose.position.x,
                robot_state.pose.position.y,
            )
            helipad_col, helipad_row = self.map.get_grid_position(
                self.destination[0],
                self.destination[1],
            )
            if robot_col == helipad_col and robot_row == helipad_row:
                self.robot_status = "Landed"
        else:
            self.robot_status = "Crashed"

    def init_robot(self, req):
        if self.robot_status == "Paused":
            self.robot_status = "Running"

        response = InitRobotResponse()
        response.success = True
        response.message = self.robot_status
        return response

    def update_robot(self):
        msg = UpdateRobot()
        msg.destination = self.destination
        return msg



class Scenario1(ScenarioParent):
    def __init__(self, robot_name, ros_setup, map):
        super(Scenario1, self).__init__(robot_name, ros_setup, map)
        self.num = 1

        start_helipad_choices = [2]
        destination_helipad_choices = [3, 4]


        self.start_helipad = start_helipad_choices[random.randint(0, len(start_helipad_choices) - 1)]
        destination_helipad = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        
        self.destination = [
            helipads[destination_helipad - 1][0],
            helipads[destination_helipad - 1][1],
        ]

        self.ros_setup.set_model_pose(
            self.robot_name,
            helipads[self.start_helipad-1][0],
            helipads[self.start_helipad-1][1],
            helipads[self.start_helipad-1][2] + 1,
        )

    def init_robot(self, req):
        response = super(Scenario1, self).init_robot(req)
        return response

    def update_robot(self):
        msg = super(Scenario1, self).update_robot()
        msg.destination = self.destination
        return msg



class Scenario2(ScenarioParent):
    def __init__(self, robot_name, ros_setup, map):
        super(Scenario2, self).__init__(robot_name, ros_setup, map)
        self.num = 2

        start_helipad_choices = [2]
        destination_helipad_choices = [3, 4]

        self.start_helipad = start_helipad_choices[random.randint(0, len(start_helipad_choices) - 1)]
        destination_helipad_1 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        destination_helipad_2 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        while destination_helipad_1 == destination_helipad_2:
            destination_helipad_2 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]


        self.destination = [
            helipads[destination_helipad_1 - 1][0],
            helipads[destination_helipad_1 - 1][1],
            helipads[destination_helipad_2 - 1][0],
            helipads[destination_helipad_2 - 1][1],
        ]

        self.ros_setup.set_model_pose(
            self.robot_name,
            helipads[self.start_helipad-1][0],
            helipads[self.start_helipad-1][1],
            helipads[self.start_helipad-1][2] + 1,
        )

    def init_robot(self, req):
        response = super(Scenario2, self).init_robot(req)
        return response

    def update_robot(self):
        msg = super(Scenario2, self).update_robot()
        msg.destination = self.destination
        return msg


class Scenario3(ScenarioParent):
    def __init__(self, robot_name, ros_setup, map):
        super(Scenario3, self).__init__(robot_name, ros_setup, map)
        self.num = 3

        start_helipad_choices = [2]
        destination_helipad_choices = [3, 4]


        self.start_helipad = start_helipad_choices[random.randint(0, len(start_helipad_choices) - 1)]
        destination_helipad_1 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        destination_helipad_2 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        while destination_helipad_1 == destination_helipad_2:
            destination_helipad_2 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]


        self.destination = [
            helipads[destination_helipad_1 - 1][0],
            helipads[destination_helipad_1 - 1][1],
            helipads[destination_helipad_2 - 1][0],
            helipads[destination_helipad_2 - 1][1],
        ]

        self.ros_setup.set_model_pose(
            self.robot_name,
            helipads[self.start_helipad-1][0],
            helipads[self.start_helipad-1][1],
            helipads[self.start_helipad-1][2] + 1,
        )

    def init_robot(self, req):
        response = super(Scenario3, self).init_robot(req)
        return response

    def update_robot(self):
        msg = super(Scenario3, self).update_robot()
        msg.destination = self.destination
        return msg


"""
class Scenario5(ScenarioParent):
    def __init__(self, robot_name, ros_setup, map):
        super(Scenario5, self).__init__(robot_name, ros_setup, map)
        self.num = 5

        start_helipad_choices = [2]
        destination_helipad_choices = [3, 4]

        self.start_helipad = start_helipad_choices[random.randint(0, len(start_helipad_choices) - 1)]
        destination_helipad_1 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        destination_helipad_2 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        while destination_helipad_1 == destination_helipad_2:
            destination_helipad_2 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]


        self.destination = [
            helipads[destination_helipad_1 - 1][0],
            helipads[destination_helipad_1 - 1][1],
            helipads[destination_helipad_2 - 1][0],
            helipads[destination_helipad_2 - 1][1],
        ]

        self.ros_setup.set_model_pose(
            self.robot_name,
            helipads[self.start_helipad-1][0],
            helipads[self.start_helipad-1][1],
            helipads[self.start_helipad-1][2] + 1,
        )

    def init_robot(self, req):
        response = super(Scenario5, self).init_robot(req)
        return response

    def update_robot(self):
        msg = super(Scenario5, self).update_robot()
        msg.destination = self.destination
        return msg


class Scenario5(ScenarioParent):
    def __init__(self, robot_name, ros_setup, map):
        self.num = 5
        self.robot_name = robot_name
        self.ros_setup = ros_setup
        self.map = map
        self.robot_status = 'Paused'
        self.init_sensors(self.robot_name)

        self.sensor_setup = DroneSensorSetupRequest()
        self.sensor_setup.gnss_status = False
        self.sensor_setup.gnss_mix_status = False
        self.sensor_setup.barometer_status = False
        self.sensor_setup.radar_status = False
        self.sensor_setup.imu_status = False
        self.sensor_setup.imu_noise = False
        self.sensor_setup.lidar_status = False
        self.sensor_setup.magnetometer_status = False
        self.sensor_setup.magnetometer_noise = False
        self.sensor_setup.battery_status = False
        self.sensor_setup.battery_heat = False
        self.sensor_setup.motor_status = False
        self.sensor_setup.motor_heat = False

        start_helipad_choices = [2]
        destination_helipad_choices = [3, 4]

        self.start_helipad = start_helipad_choices[random.randint(0, len(start_helipad_choices) - 1)]
        destination_helipad_1 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        self.destination_helipad_2 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        while destination_helipad_1 == self.destination_helipad_2:
            self.destination_helipad_2 = destination_helipad_choices[random.randint(0,len(destination_helipad_choices) - 1)]
        
        self.destination = [
            helipads[destination_helipad_1 - 1][0],
            helipads[destination_helipad_1 - 1][1],
        ]

        self.time_passed = 0.0
        self.start_time = 0.0

        self.target_switch_time = random.uniform(15.0, 25.0)

        self.ros_setup.set_model_pose(
            self.robot_name,
            helipads[self.start_helipad-1][0],
            helipads[self.start_helipad-1][1],
            helipads[self.start_helipad-1][2] + 1,
        )

    def on_contact(self, data):
        if "helipad" in data.data:
            robot_state = self.ros_setup.get_model_state("quadrotor", "")
            robot_col, robot_row = self.map.get_grid_position(
                robot_state.pose.position.x,
                robot_state.pose.position.y,
            )
            helipad_col, helipad_row = self.map.get_grid_position(
                self.destination[0],
                self.destination[1],
            )
            if robot_col == helipad_col and robot_row == helipad_row:
                self.robot_status = "Landed"
        else:
            self.robot_status = "Crashed"

    def update_robot(self, req):      
        if self.robot_status == "Paused":
            self.robot_status = "Running"

        self.time_passed = rospy.get_time() - self.start_time

        if self.time_passed > self.target_switch_time:
            self.destination = [
                helipads[self.destination_helipad_2 - 1][0],
                helipads[self.destination_helipad_2 - 1][1],
            ]

        response = ScenarioResponse()
        response.success = True
        response.message = self.robot_status
        response.destination = self.destination
        response.helipads = self.map.helipad_grid

        return response
"""